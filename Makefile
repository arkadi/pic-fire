CC = gcc
LXRT_CFLAGS = $(shell /usr/realtime/bin/rtai-config --lxrt-cflags)
LXRT_LDFLAGS = $(shell /usr/realtime/bin/rtai-config --lxrt-ldflags)

all: hex emu

hex: pic-fire.hex

emu: tdc-rtai

tdc: tdc.c
	$(CC) -Wall -O -o $@ $< -lcurses

tdc-rtai: tdc-rtai.c
	$(CC) -g -Wall -O $(LXRT_CFLAGS) -o $@ $< -lcurses $(LXRT_LDFLAGS)

pic-fire.hex: pic-fire.o
	gplink -w -r -c -o $@ -m $^ -I /usr/share/sdcc/lib/pic libsdcc.lib pic16f628a.lib

pic-fire.o: pic-fire.asm
	gpasm -L -c $<

pic-fire.asm: pic-fire.c Makefile
	sdcc -S --nooverlay --opt-code-size --disable-warning 84 -mpic14 -p16f628a $<
#	perl -pi~ -e 's/idata\s+0x2100/code\t0x2100/' $@

install: pic-fire.hex
	picprog --erase --rdtsc --burn -i $< -d pic16f628a -p /dev/ttyS0

clean:
	@rm -f pic-fire*.{hex,asm,as,o,cod,cof,map,lst,obj,rlf,hxl,sym} tdc tdc-rtai *~
