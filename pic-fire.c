#ifdef SDCC
#include <pic/pic16f628a.h>

typedef unsigned int config;
config __at 0x2007 __CONFIG =
    _WDT_OFF &              // watchdog reset
    _PWRTE_OFF &            // power-up timer
  //_INTOSC_OSC_NOCLKOUT &  // internal oscillator
    _HS_OSC &               // 8-10MHz high speed crystal/resonator
    _MCLRE_ON &             // master clear reset pin
    _BOREN_ON &             // brown-out reset
    _LVP_OFF &              // low-voltage programming
    _CP_OFF &               // code protection
    _DATA_CP_OFF;           // data protection

//typedef unsigned char eeprom;
//__code eeprom __at 0x2100 __EEPROM[] = { 0xAA, 0x1E, 0x00, 0x01, 0x0A, 0x64, 0x1E, 0x01 };

#else // Hi-Tech PICC Lite
#include <pic.h>

__CONFIG(WDTDIS & PWRTDIS & HS & MCLREN & BOREN & LVPDIS);

#define OPTION_REG OPTION

#endif

#define LED_PIN       RA2

#define cam_trigger   RA0
#define crank_trigger RA1
#define spark_trigger RA3

#define YES    2
#define SEARCH 1
#define NO     0

#define TMR0_PER_MS 20 // TMR0_PER_MS/(20MHz/4 clocks per cycle/256 prescaler) = 0.001s (1ms)
#define MAX_DWELL   (TMR0_PER_MS*7) // 7ms
#define FLASH_TIME  800

// the main loop events log is sent via usart
#define START      'S'
#define CURR_CYL   'n'
#define NEXT_CYL   'N'
#define WAIT_LOW_STARTED   'w'
#define WAIT_LOW_COMPLETED 'W'
#define BAD_DWELL  'L'
#define HALT       'H'
#define SYNCED     'Y'
#define WAIT_SYNC  'y'
#define CRANK_HIGH 'K'
#define CRANK_LOW  'k'
#define CAM_HIGH   'M'
#define CAM_LOW    'm'
#define RESTART_SEARCH 'R'

// transmit a byte via usart
void tx(char c) {
    while (!TXIF); // wait for transmitter to become free
    TXREG = c;
}

void txnl(void) {
    tx('\r'); tx('\n');
}

void txnlc(char c) {
    txnl(); tx(c); txnl();
}

unsigned char start_timer(void)
{
    unsigned char tmr;
    T0IF = 0;
    // here is a short window of TMR0 overflow
    tmr = TMR0;
    if (tmr != 255)
        T0IF = 0;
    return tmr;
}

int time_since(unsigned char tmr_start)
{
    int tm;
    if (T0IF) { // if timer overflow
        tm = 255 - tmr_start + TMR0;
    } else {
        tm = TMR0 - tmr_start;
        if (tm < 0) // overflow happened just after T0IF is read
            tm = 255 - tmr_start;
    }
    return tm;
}

void delay_ms(unsigned int ms)
{

    unsigned char tmr_start, debug_skip_i = 0;
    while (ms--) {
        tmr_start = start_timer();
        if (!debug_skip_i++)
            tx('d');
        while (time_since(tmr_start) < TMR0_PER_MS);
    }
/*
    int i;
    while (ms--) for (i = 0; i < 440; ++i);
*/
}

// to flash the led
volatile unsigned int flash = 0;

// cylinders 1..6 PORTB masks for wasted spark 2+5, 3+6, 1+4 in that order
// after cylinder 1 crank goes low it is 75 degrees BTDC cylinder 2
// and the spark should be for cylinder 2, obviously
// spark 1+4 - RB0
// spark 2+5 - RB3
// spark 3+6 - RB5
const char sparks[] = { 0x08, 0x20, 0x01, 0x08, 0x20, 0x01 };

void main()
{
    unsigned char synced = NO, crank_trigger_prev, crank_raised_when_cam_active, cyl_no, cyl_no_prev, sync_search_i;
    unsigned char spark_trigger_prev, spark_no, spark_start, dwell_no;
    unsigned char debug_skip_i = 0;
    int dwell;

    TRISA = 0xFB; // RA4-7 is input/high-impedance output (1), RA2 (led) is output (0), RA0,1,3 are inputs
    CMCON = 0x07; // disable comparators
    TRISB = 0xD6; // RA0,3,5 are outputs, other pins are inputs
    T0IE = 0; // TMR0 interrupt disable, default on POR, but set it anyway
    OPTION_REG = 0xC7; // RB pull-ups are disabled, TMR0 transition on internal clock, prescaler assigned to TMR0 with a rate 256
    // usart at async 115.2kbps
    TXIE = 0; // TXREG free interrupt disable
  //SPBRG = 64; // 19.2kbps
    SPBRG = 10; // 115.2kbps
    TXSTA = 0x24; // TXEN = 1; SYNC = 0; BRGH = 1; everything else is 0
    RCSTA = 0x90; // SPEN = 1; CREN = 1; everything else is 0
    RCIE = 1; // receiver interrupt enable

    txnlc(START);
    while (1) {
        /*
        LED_PIN = 0;
        delay_ms(1000);
        LED_PIN = 1;
        delay_ms(1000);
        continue;
        */
        /*
        if (cam_trigger)
            LED_PIN = 0;
        else
            LED_PIN = 1;
        continue;
        */
        if (synced == YES) {
            // output spark signal
            if (spark_trigger) {
                if (!spark_trigger_prev) { // start charging the coil
                    spark_trigger_prev = 1;
                    // at high RPM the coil is started charging before next crank trigger arrives
                    // don't charge the same coil twice, assume next cylinder
                    if (cyl_no == spark_no) {
                        flash = FLASH_TIME; // XXX debug
                        spark_no = cyl_no + 1;
                        spark_no = (spark_no > 6) ? 1 : spark_no;
                        tx(NEXT_CYL);
                    } else {
                        spark_no = cyl_no;
                        tx(CURR_CYL);
                    }
                    tx(spark_no + 'A' - 1);
                    PORTB = sparks[spark_no-1];
                    // remember the moment the coil charge is started to check the dwell
                    spark_start = start_timer();
                } else if (spark_no != dwell_no) { // check the dwell
                    dwell = time_since(spark_start);
                    if (dwell > MAX_DWELL) {
                        dwell_no = spark_no;
                      //flash = FLASH_TIME;
                      //tx(BAD_DWELL);
                    }
                }
            } else {
                PORTB = 0;
                spark_trigger_prev = 0;
            }
            // flash the led for some time, even when already synced
            if (flash) {
                --flash;
                LED_PIN = 0;
            } else
                LED_PIN = 1;
        } else {
            if (!debug_skip_i++)
                tx(WAIT_SYNC);
            PORTB = 0;
            LED_PIN = 0;
            if (synced != SEARCH) {
                tx(WAIT_LOW_STARTED);
                while (crank_trigger || cam_trigger); // loop until both signals are low
                tx(WAIT_LOW_COMPLETED);
                synced = SEARCH;
                crank_trigger_prev = 0;
                spark_trigger_prev = 0;
                cyl_no = 0;
                cyl_no_prev = 0;
                sync_search_i = 0;
                spark_no = 0;
                dwell_no = 0;
            }
        }

        if (crank_trigger) {
            if (!crank_trigger_prev) { // crank low -> high: remember cam status
                crank_trigger_prev = 1;
                crank_raised_when_cam_active = cam_trigger;
                tx(CRANK_HIGH); tx(crank_raised_when_cam_active ? CAM_HIGH : CAM_LOW);
            }
        } else {
            if (crank_trigger_prev) { // crank high -> low: perform the checks
                crank_trigger_prev = 0;
                tx(CRANK_LOW); tx(cam_trigger ? CAM_HIGH : CAM_LOW);
                // there could be inconsistence between event log and actual programm flow if CAM signal is lowered right now, but it is not possible on 6g72
                if (cam_trigger) {
                    if (crank_raised_when_cam_active) { // this happens at cylinder 1 only
                        if (synced == YES) {
                            if (cyl_no == 6)
                                cyl_no = 1;
                            else
                                synced = NO;
                        } else
                            cyl_no = 1;
                    } else { // cylinder 2 or 5
                        if (cyl_no == 1 || cyl_no == 4)
                            ++cyl_no;
                        else if (synced == YES)
                            synced = NO;
                    }
                } else { // cam trigger is low
                    if (crank_raised_when_cam_active) { // this happens at cylinder 4 only
                        if (synced == YES) {
                            if (cyl_no == 3)
                                cyl_no = 4;
                            else
                                synced = NO;
                        } else
                            cyl_no = 4;
                    } else { // cylinder 3 or 6
                        if (cyl_no == 2 || cyl_no == 5)
                            ++cyl_no;
                        else if (synced == YES)
                            synced = NO;
                    }
                }
                // format event log output
                if (synced == YES && cyl_no == 1)
                    txnl();
                tx(cyl_no + '0'); // XXX delay here
                // if not synced, then search for 6 successfull (in order, starting from cylinder 1 or 4) cylinder events and enter synced mode when found
                if (synced == SEARCH && cyl_no != 0) {
                    if (cyl_no_prev != 0) {
                        if (cyl_no_prev == ((cyl_no == 1) ? 6 : cyl_no - 1))
                            ++sync_search_i;
                        else {
                            synced = NO;
                            tx(RESTART_SEARCH);
                        }
                    }
                    cyl_no_prev = cyl_no;
                    if (sync_search_i == 5) { // not 6, because there was no increment for first recognized cylinder
                        synced = YES;
                        txnlc(SYNCED);
                        debug_skip_i = 0;
                    }
                } else if (synced == NO) {
                    // shutdown gracefully by allowing engine to stop
                    tx(HALT);
                    delay_ms(5000);
                }
            }
        }
    }
}

#ifdef SDCC
void isr(void) __interrupt(0) {
#else
interrupt isr(void) {
#endif
    while (RCIF) {
        char c = RCREG;
    }
    // reset input overrun
    if (OERR) {
        flash = FLASH_TIME;
        CREN = 0;
        CREN = 1;
    }
}
