#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/io.h>
#include <signal.h>

#include <rtai_lxrt.h>

#include <curses.h>

#define LPTPORT 0x378 // lpt1
// the pins of parallel port
#define SPARK_PIN 1
#define CRANK_PIN 2
#define CAM_PIN   3
// masks
#define SPARK_HIGH (1 << (SPARK_PIN-1))
#define CRANK_HIGH (1 << (CRANK_PIN-1))
#define CAM_HIGH   (1 << (CAM_PIN-1))
#define SPARK_LOW  (~(1 << (SPARK_PIN-1)))
#define CRANK_LOW  (~(1 << (CRANK_PIN-1)))
#define CAM_LOW    (~(1 << (CAM_PIN-1)))

// signal to exit from Ctrl-C or key event loop
int must_exit = 0;
// ignition coil charge parameters
int spark_start = 80, spark_end = 110;
// the increment of crank rotation per tick
const int inc = 5; // degrees
// the tdc() period
RTIME period;

void *tdc(void *arg)
{
    RT_TASK *me;
    // gradual adjustment of task period when engine speed is changed
    RTIME period_target, period_curr;
    int period_step = 0, period_adj_i = 0;
    // output byte
    char v = 0;
    int angle, spark_start_curr, spark_end_curr, spark_angle;

    if (!(me = rt_thread_init(nam2num("tdc"), 0, 0, SCHED_FIFO, -1))) {
        fprintf(stderr, "rt_thread_init_schmod() failed: %s\n", strerror(errno));
        exit(1);
    }
    rt_allow_nonroot_hrt();
    rt_make_hard_real_time();

    period_target = period_curr = period;
    while (1) {
        if (must_exit)
            break;
        spark_start_curr = spark_start;
        spark_end_curr = spark_end;
        for (angle = 0; angle < 720; angle += inc) {
            switch (angle) {
                // cam
                case 70:  case 190: case 380: case 550: v &= CAM_LOW; break;  // cam goes low
                case 150: case 340: case 510: case 705: v |= CAM_HIGH; break; // cam goes high
                // crank
                case 45:  case 165: case 285: case 405: case 525: case 645: v &= CRANK_LOW; break;  // crank goes low
                case 115: case 235: case 355: case 475: case 595: case 715: v |= CRANK_HIGH; break; // crank goes high
            }
            // spark
            spark_angle = angle%120;
            if (spark_angle == spark_start_curr || spark_angle == spark_start_curr + 120 || angle >= 720 + spark_start /* next cycle spark */)
                v |= SPARK_HIGH;
            else if (spark_angle == spark_end_curr || spark_angle == spark_end_curr - 120)
                v &= SPARK_LOW;

            outb(v, LPTPORT);

            // gradually adjust to desired rpms
            if (period != period_target) {
                period_target = period;
                period_adj_i = 1000000000/period_curr;
                if (period_adj_i > 0) {
                    period_step = (period_target - period_curr)/period_adj_i;
                    ++period_adj_i;
                } else
                    period_adj_i = 1;
            }
            if (period_adj_i) {
                if (period_adj_i == 1) {
                    period_adj_i = 0;
                    period_curr = period_target;
                } else {
                    --period_adj_i;
                    period_curr += period_step;
                }
            }

            rt_task_make_periodic_relative_ns(me, 0, period_curr);
            rt_task_wait_period();
        }
    }
    outb(0, LPTPORT);
    rt_make_soft_real_time();
    rt_task_delete(me);
    return NULL;
}

void stop(int x)
{
    must_exit = 1;
}

RTIME rpm2period(int rpm)
{
    return 1000000000/((float)rpm/60)/(360/inc); // (1 s == 10e9 ns)/(rpm/60 s)/(360 degrees per revolution/increment per tick == 5 degrees)
}

int main(int argc, char **argv)
{
    RT_TASK *task;
    long tdc_thread;
    int ch; // currently read keyboard char
    int update = 1; // need screen update
    int rpm = 800; // start at 800rpm
    int rpm_step;

    // lpt port permissions
    if (ioperm(LPTPORT, 1, 1)) {
        fprintf(stderr, "ioperm() failed: %s\n", strerror(errno));
        return 1;
    }
    // rtai init - let the main() also be a rtai backed thread, just in case
    if (!(task = rt_task_init(nam2num("main"), 1, 0, 0))) { // priority of 1, default stack and message size, SCHED_FIFO, and all CPU-s
        fprintf(stderr, "rt_task_init() failed: %s\n", strerror(errno));
        return 1;
    }
    period = rpm2period(rpm);
    rt_set_oneshot_mode();
    start_rt_timer(0);
  //start_rt_timer(nano2count(period)); // for periodic time mode
    tdc_thread = rt_thread_create(tdc, NULL, 0); // NULL arg and default stack size
    // ncurses init
    initscr(); cbreak(); noecho(); nonl(); /* nodelay(stdscr, TRUE); */ intrflush(stdscr, FALSE); keypad(stdscr, TRUE);
    signal(SIGINT, stop); // exit gracefully on Ctrl-C
    // XXX at Ctrl-C tdc() stops but not the main thread until the key is pressed
    while (1) {
        if (must_exit)
            break;
        if (update) {
            printw("\rrpm:%5d; spark start:%3d; end: %3d", rpm, spark_start, spark_end);
            refresh();
        }
        update = 1;
        switch ((ch = getch())) {
            default:
            case ERR: update = 0; break;
            // exit
            case 'x': case 'X': case 'q': case 'Q': must_exit = 1; break;
            // lower or raise engine rpms
            case ',': case '.':
                if (rpm < 1000)      rpm_step = 10;
                else if (rpm < 2000) rpm_step = 50;
                else                 rpm_step = 100;

                if (ch == ',') { if (rpm > 10)    rpm -= rpm_step; }
                else           { if (rpm < 20000) rpm += rpm_step; }

                period = rpm2period(rpm);
                break;
            // set spark timing
            case KEY_LEFT:  if (spark_start > -25 && spark_start > spark_end + 5 - 120)  spark_start -= inc; break;
            case KEY_RIGHT: if (                     spark_start < spark_end - 5)        spark_start += inc; break;
            case KEY_DOWN:  if (spark_end > 80    && spark_end > spark_start + 5)        spark_end   -= inc; break;
            case KEY_UP:    if (spark_end < 130   && spark_end < spark_start - 5 + 120)  spark_end   += inc; break;
        }
    }

    endwin();

    rt_thread_join(tdc_thread);
    stop_rt_timer();
    rt_task_delete(task);

    return 0;
}
